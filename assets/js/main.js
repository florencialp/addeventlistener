//EJEMPLO 1

let btnFormulario = document.getElementById("btnFormulario");
let modal = document.getElementById("modal");


btnFormulario.addEventListener("click", function(e) {
    e.preventDefault();
    mostrarDatosForm();
});


function mostrarDatosForm() {
    let datoForm = document.getElementById("datoForm");
    let nombre = document.getElementById("nombre");
    let serie = document.getElementById("serie");

    if (nombre.value == "" && serie.value == "") {
        datoForm.innerHTML = `sos una persona y no tenes una serie favorita`;

    } else if (serie.value == "") {
        datoForm.innerHTML = `sos ${nombre.value} y no tenes una serie favorita`;

    } else if (nombre.value == "") {
        datoForm.innerHTML = `sos una persona y tu serie favorita es ${serie.value}`;

    } else {
        datoForm.innerHTML = `sos ${nombre.value} y tu serie favorita es ${serie.value}`;
    }
}



//EJEMPLO 2
let btnTexto = document.getElementById("btnTexto");
let btnImagen = document.getElementById("btnImagen");
let btnEmoji = document.getElementById("btnEmoji");


btnTexto.addEventListener("mouseover", function() {
    let datoTexto = document.getElementById("datoTexto");
    datoTexto.innerHTML = "hola! soy un texto";
});

btnImagen.addEventListener("click", function() {
    let datoImagen = document.getElementById("datoImagen");
    datoImagen.classList = "show";
    datoImagen.setAttribute("src", "./assets/img/imagen.jpg");
});

btnEmoji.addEventListener("mouseout", function() {
    let datoEmoji = document.getElementById("datoEmoji");
    datoEmoji.innerHTML = '<i class="em em-cherry_blossom" aria-role="presentation" aria-label="CHERRY BLOSSOM"></i><i class="em em-sunflower" aria-role="presentation" aria-label="SUNFLOWER"></i>';
});